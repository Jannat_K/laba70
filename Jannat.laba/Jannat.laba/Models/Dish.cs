﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Jannat.laba.Models
{
    public class Dish
    {

       
        public string Meal_name { get; set; }
        public int Price { get; set; }
        public string Meal_discription { get; set; }


        public int CafeId { get; set; }
        public virtual Cafe Cafe { get; set; }

       
    }
}
