﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Jannat.laba.Models
{
    public class Cafe
    {
       
        public string Name { get; set; }
        public string Picture { get; set; }
        public string Discription { get; set; }

        public virtual ICollection<Dish> Dishes { get; set; }

        public Cafe()
        {
            Dishes = new List<Dish>();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
